## Java Collections Cookbooks and Examples

This module contains articles about conversions among Collection types and arrays in Java.

### Relevant Articles: 

- [Java Array 和 String 的转换](https://www.ossez.com/t/java-array-string/13685)
- [Mapping Lists with ModelMapper](https://www.baeldung.com/java-modelmapper-lists)
- [Converting List to Map With a Custom Supplier](https://www.baeldung.com/list-to-map-supplier)
- [Java Arrays.asList 和 new ArrayList(Arrays.asList()) 的对比](https://www.ossez.com/t/java-arrays-aslist-new-arraylist-arrays-aslist/13680)
- [Iterate Over a Set in Java](https://www.baeldung.com/java-iterate-set)
- More articles: [[<-- prev]](../java-collections-conversions)
