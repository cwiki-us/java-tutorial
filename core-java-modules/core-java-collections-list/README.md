## Java 核心（Core Java）集合中的 List 列表

这个模块包含有 Java List 集合有关的文章和内容

### 相关文章： 
- [Java 如何从一个 List 中随机获得元素](https://www.ossez.com/t/java-list/13934)
- [Removing all nulls from a List in Java](http://www.baeldung.com/java-remove-nulls-from-list)
- [Removing all duplicates from a List in Java](http://www.baeldung.com/java-remove-duplicates-from-list)
- [How to TDD a List Implementation in Java](http://www.baeldung.com/java-test-driven-list)
- [Iterating Backward Through a List](http://www.baeldung.com/java-list-iterate-backwards)
- [如何从 Java 的 List 中删除第一个元素](https://www.ossez.com/t/java-list/13919)
- [How to Find an Element in a List with Java](http://www.baeldung.com/find-list-element-java)
- [Finding Max/Min of a List or Collection](http://www.baeldung.com/java-collection-min-max)
- [Remove All Occurrences of a Specific Value from a List](https://www.baeldung.com/java-remove-value-from-list)
- [[Next -->]](/core-java-modules/core-java-collections-list-2)
